﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    public class Channel
    {
        [JsonProperty("data")]
        public UserData[] UserData { get; set; }
    }

    public partial class UserData
    {
        [JsonProperty("broadcaster_id")]
        public long BroadcasterId { get; set; }

        [JsonProperty("broadcaster_login")]
        public string BroadcasterLogin { get; set; }

        [JsonProperty("broadcaster_name")]
        public string BroadcasterName { get; set; }

        [JsonProperty("broadcaster_language")]
        public string BroadcasterLanguage { get; set; }

        [JsonProperty("game_id")]
        public long GameId { get; set; }

        [JsonProperty("game_name")]
        public string GameName { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("delay")]
        public long Delay { get; set; }
    }
}
