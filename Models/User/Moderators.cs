﻿using Newtonsoft.Json;
using System.Collections.Generic;


namespace Models
{
    public class ModeratorData
    {
        [JsonProperty("user_id")]
        public string UserId { get; set; }

        [JsonProperty("user_login")]
        public string UserLogin { get; set; }

        [JsonProperty("user_name")]
        public string UserName { get; set; }
    }


    public class Moderators
    {
        [JsonProperty("data")]
        public List<ModeratorData> Mods { get; set; }

        [JsonProperty("pagination")]
        public Pagination Pagination { get; set; }
    }
}
