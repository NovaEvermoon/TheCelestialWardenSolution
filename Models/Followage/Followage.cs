﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    public class FollowageData
    {
        public string from_id { get; set; }
        public string from_login { get; set; }
        public string from_name { get; set; }
        public string to_id { get; set; }
        public string to_login { get; set; }
        public string to_name { get; set; }
        public DateTime followed_at { get; set; }
    }

    public class Followage
    {
        public int Total { get; set; }
        public List<FollowageData> Data { get; set; }
        public Pagination Pagination { get; set; }
    }
}
