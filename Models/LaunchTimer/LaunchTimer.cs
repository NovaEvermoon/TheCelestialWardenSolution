﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Timers;
using Utils;

namespace Models
{
    public class LaunchTimer
    {
        public List<CustomTimer> Timers;
        public IrcClient Irc;
        public string FunctionName;
        public string AssemblyName;
        public string[] Parameters;
        public int Miliseconds;
        public int Result;

        public LaunchTimer(string _FunctionName, string _AssemblyName, string[] _Parameters, int _Miliseconds)
        {
            AssemblyName = _AssemblyName;
            FunctionName = _FunctionName;
            Parameters = _Parameters;
            Miliseconds = _Miliseconds;

            Timers = new List<CustomTimer>();
        }

        public LaunchTimer(IrcClient _Irc)
        {
            Irc = _Irc;
            Timers = new List<CustomTimer>();
        }

        public void CreateTimer(Command _Command)
        {

            CustomTimer timer = new CustomTimer(_Command.Timer, _Command.Message);
            timer.Start();
            timer.Elapsed += new ElapsedEventHandler(Timer_Elapsed);
            Timers.Add(timer);
        }

        public void Timer_Elapsed_With_Result(object _Sender, ElapsedEventArgs _E)
        {
            if (_Sender != null)
                if (_Sender is Timer)
                {
                    try
                    {
                        MethodInfo mInfo;
                        Type type = Assembly.Load("MoonBot_Data").GetType(AssemblyName, false, true);
                        mInfo = type.GetMethod(FunctionName);

                        object answer = mInfo.Invoke(null, Parameters);
                        Result = Convert.ToInt32(answer);
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex);
                    }
                }
        }

        public void Timer_Elapsed(object _Sender, ElapsedEventArgs _E)
        {
            if (_Sender != null)
                if (_Sender is CustomTimer timer)
                    Irc.WriteChatMessage(timer.CommandMessage);
        }
    }

    public class CustomTimer : Timer
    {
        public string CommandMessage;
        public User User;

        public CustomTimer(double _Miliseconds) : base(_Miliseconds)
        {

        }

        public CustomTimer(double _Miliseconds, string _CommandMessage) : base(_Miliseconds)
        {
            CommandMessage = _CommandMessage;
        }

        public CustomTimer(double _Miliseconds, User _User) : base(_Miliseconds)
        {
            User = _User;
        }
    }
}
