﻿using System.Web.Configuration;

namespace Models
{
    public class ChatBot
    {
        public  string BotName { get; set; }
        public  string ClientId { get; set; }
        public  string AccessToken { get; set; }
        public  string Secret { get; set; }
        public  string Scopes { get; set; }


        public ChatBot(string _BotName, string _ClientId, string _AccessToken,string _Secret, string _Scopes)
        {
            this.BotName = _BotName;
            this.ClientId = _ClientId;
            this.AccessToken = _AccessToken;
            this.Secret = _Secret;
            this.Scopes = _Scopes;
        }


        public static string GetMessage(string _FullMessage)
        {
            string message;
            message = _FullMessage.Substring(_FullMessage.IndexOf('#'));
            message = message.Substring(message.IndexOf(':') + 1);

            return message;
        }
    }

    
}
