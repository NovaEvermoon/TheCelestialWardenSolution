﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Models
{

    public class SubscriptionData
    {
        [JsonProperty("broadcaster_id")]
        public string BroadcasterId { get; set; }

        [JsonProperty("broadcaster_name")]
        public string BroadcasterName { get; set; }

        [JsonProperty("broadcaster_login")]
        public string BroadcasterLogin { get; set; }

        [JsonProperty("is_gift")]
        public bool IsGift { get; set; }

        [JsonProperty("tier")]
        public string Tier { get; set; }
    }

    public class Subscription
    {
        [JsonProperty("data")]
        public List<SubscriptionData> Subs { get; set; }
    }
}
