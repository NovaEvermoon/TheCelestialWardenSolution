﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;

namespace Models
{
    public class Command
    {
        public int Id { get; set; }
        public string Keyword { get; set; }
        public string Message { get; set; }
        public string UserLevel { get; set; }
        public int Cooldown { get; set; }
        public bool Status { get; set; }
        public int Timer { get; set; }
        public string Description { get; set; }
        public DateTime StartedTime { get; set; }
        public string Type { get; set; }
        public string Request { get; set; }
        public int Parameters { get; set; }
        public Dictionary<string, dynamic> ParameterList { get; set; }
        public string Assembly { get; set; }
        public string Condition { get; set; }

        public Command()
        {
            ParameterList = new Dictionary<string, dynamic>();
        }

        public static bool IsKappamonCommand(string _CommandMessage)
        {
            string[] kappamonCommands = { "song", "feed", "meow" };
            bool exists = false;

            if (kappamonCommands.Contains(_CommandMessage))
            {
                exists = true;
            }
            return exists;
        }

        
    }
}
