﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Utils
{
    public class PingSender
    {
        private IrcClient ircClient;
        private Thread pingSender;

        public PingSender(IrcClient _IrcClient)
        {
            ircClient = _IrcClient;
            pingSender = new Thread(new ThreadStart(Run));
        }

        public void Start()
        {
            pingSender.IsBackground = true;
            pingSender.Start();
        }

        public void Run()
        {
            while (true)
            {
                Console.WriteLine($"{DateTime.Now:dd-MM-yyyy} - Sending PING");
                ircClient.SendIrcMessage("PING irc.twitch.tv");
                Thread.Sleep(300000);
            }
        }
    }
}
