﻿

using System.Text.RegularExpressions;

namespace Utils
{
    public static class Bot
    {
        public static bool CheckLink(string message)
        {
            bool link = false;
            Regex regex = new Regex(@"\b(?:https?://|www\.)\S+\b", RegexOptions.Compiled | RegexOptions.IgnoreCase);

            foreach (Match m in regex.Matches(message))
            {
                if (m != null)
                {
                    link = true;
                }
            }

            return link;
        }
    }
}
