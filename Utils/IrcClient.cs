﻿using System;
using System.IO;
using System.Net.Sockets;
using System.Text;


namespace Utils
{
    public class IrcClient
    {
        public string UserName;
        public string Channel;

        public TcpClient TcpClient;
        public StreamReader InputStream;
        public StreamWriter OutputStream;

        public IrcClient(string _Ip, int _Port, string _UserName, string _Password, string _Channel)
        {
            try
            {
                UserName = _UserName.ToLower();
                Channel = _Channel.ToLower();

                TcpClient = new TcpClient(_Ip, _Port);
                InputStream = new StreamReader(TcpClient.GetStream());
                OutputStream = new StreamWriter(TcpClient.GetStream());

                OutputStream.WriteLine($":{UserName}!{UserName}@{UserName}.tmi.twitch.tv PASS oauth:{_Password}");
                OutputStream.WriteLine($":{UserName}!{UserName}@{UserName}.tmi.twitch.tv NICK {UserName}");
                OutputStream.WriteLine($":{UserName}!{UserName}@{UserName}.tmi.twitch.tv USER {UserName} 8 * {UserName}");
                OutputStream.WriteLine($":{UserName}!{UserName}@{UserName}.tmi.twitch.tv JOIN #{Channel}");
                OutputStream.Flush();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public void SendIrcMessage(string _Message)
        {
            OutputStream.WriteLine(_Message);
            OutputStream.Flush();
        }

        public string ReadMessage()
        {
            try
            {
                Console.WriteLine($"{DateTime.Now:dd-MM-yyyy} - Reading message");
                string message = InputStream.ReadLine();
                Console.WriteLine($"{DateTime.Now:dd-MM-yyyy} - Message : {message}");
                return message;
            }
            catch (Exception ex)
            {
                string message = $"{DateTime.Now:dd-MM-yyyy} - Error receiving message: {ex.Message}";
                Console.WriteLine(message);
                return message;
            }
        }

        public void WriteChatMessage(string _Message)
        {
            try
            {
                OutputStream.WriteLine($":{UserName}!{UserName}@{UserName}.tmi.twitch.tv PRIVMSG #{Channel}  : {_Message}");
                OutputStream.Flush();
            }
            catch (Exception ex)
            {
                StringBuilder sb = new StringBuilder($"{DateTime.Now:dd - MM - yyyy}  :  {ex.Message}");
                Console.WriteLine(sb);
            }
        }

    }
}
