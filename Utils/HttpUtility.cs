﻿using System.Net;

namespace Utils
{
    public static class HttpUtility
    {
        public static string Encode(string _Message)
        {
            string encodedMessage = WebUtility.UrlEncode(_Message);

            return encodedMessage;
        }
    }
}
