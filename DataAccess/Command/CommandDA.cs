﻿using Models;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Text;
using System.Web.Configuration;

namespace DataAccess
{
    public static class CommandDA
    {
        public static List<Command> LoadCommands()
        {
            string connectionString = WebConfigurationManager.ConnectionStrings["MysqlMoonBotDataBase"].ConnectionString;
            List<Command> commands = new List<Command>();

            try
            {
                using (MySqlConnection mySqlConnection = new MySqlConnection(connectionString))
                {
                    mySqlConnection.Open();
                    string query = "SELECT * FROM command WHERE command_status != 0";
                    MySqlCommand command = new MySqlCommand(query, mySqlConnection);
                    MySqlDataReader reader = command.ExecuteReader();

                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            Command chatCommand = new Command();
                            try
                            {
                                chatCommand.Id = reader.GetInt32(0);
                                chatCommand.Keyword = reader.GetString(1);
                                chatCommand.Message = reader.GetString(2);
                                chatCommand.UserLevel = reader.GetString(3);
                                chatCommand.Cooldown = reader.GetInt32(4);
                                chatCommand.Status = reader.GetBoolean(5);
                                chatCommand.Timer = reader.GetInt32(6);
                                chatCommand.Description = reader.GetString(7);
                                chatCommand.Type = reader.GetString(8);
                                chatCommand.Request = reader.GetString(9);
                                chatCommand.Parameters = reader.GetInt32(10);

                                string commandParameters = reader.GetString(11);
                                if (commandParameters != "")
                                {
                                    string[] cmdParams = commandParameters.Split('|');

                                    for (int i = 0; i < cmdParams.Length; i++)
                                    {
                                        chatCommand.ParameterList.Add(cmdParams[i], "");
                                    }
                                }

                                chatCommand.Assembly = reader.GetString(12);
                                chatCommand.Condition = reader.GetString(13);
                                commands.Add(chatCommand);
                            }
                            catch (Exception ex)
                            {
                                string errorMessage = $"{DateTime.Now:dd-MM-yyyy} Error loading commands : {ex.Message}";
                                Console.WriteLine(errorMessage);
                            }

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                StringBuilder sb = new StringBuilder(DateTime.Now.ToString("dd-MM-yyyy") + " : " + ex.Message);
                Console.WriteLine(sb);
            }

            return commands;
        }

        public static Tuple<int, string> ExecuteSelectCommand(string _Query)
        {
            string connectionString = ConfigurationManager.ConnectionStrings["MysqlMoonBotDataBase"].ConnectionString;
            Tuple<int, string> answer = null;

            using (MySqlConnection mySqlConnection = new MySqlConnection(connectionString))
            {
                mySqlConnection.Open();
                MySqlCommand mySqlCommand = new MySqlCommand(_Query, mySqlConnection);
                var result = mySqlCommand.ExecuteScalar();

                mySqlConnection.Close();
                if (result == null)
                {
                    answer = new Tuple<int, string>(0, "There was a problem executing that command");
                }
                else
                {
                    answer = new Tuple<int, string>(Convert.ToInt32(result), "");
                }

            }

            return answer;
        }

        public static Tuple<int, string> ExecuteUpdateCommand(string _Query, string _Message)
        {
            string connectionString = ConfigurationManager.ConnectionStrings["MysqlMoonBotDataBase"].ConnectionString;
            Tuple<int, string> answer = null;

            using (MySqlConnection mySqlConnection = new MySqlConnection(connectionString))
            {
                mySqlConnection.Open();
                MySqlCommand mySqlCommand = new MySqlCommand(_Query, mySqlConnection);
                int result = mySqlCommand.ExecuteNonQuery();
                mySqlConnection.Close();
                if (result < 0)
                {
                    answer = new Tuple<int, string>(result, "There was an error executing this request");
                }
                else
                {
                    answer = new Tuple<int, string>(result, _Message);
                }
            }

            return answer;
        }
    }
}
