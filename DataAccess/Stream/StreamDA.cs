﻿using Models;
using Newtonsoft.Json;
using System;
using System.IO;
using System.Net;
using System.Text;
using ResponseStream = System.IO.Stream;
using Stream = Models.Stream;

namespace DataAccess
{
    public static class StreamDA
    {
        public static Stream GetStreamByChannelName(string _ClientId, string _ChannelName, string _Token)
        {
            Stream Stream = new Stream();

            string Url = $"https://api.twitch.tv/helix/streams?user_login={_ChannelName}";

            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(Url);
                if (request != null)
                {
                    request.Method = "GET";
                    request.Timeout = 12000;
                    request.ContentType = "application/json";
                    request.Accept = "Accept: application/vnd.twitchtv.v5+json";
                    request.Headers.Add($"Client-ID: {_ClientId}");
                    request.Headers.Add($"Authorization: Bearer {_Token}");
                }

                using (ResponseStream s = request.GetResponse().GetResponseStream())
                {
                    using (StreamReader sr = new StreamReader(s))
                    {
                        string jsonResponse = sr.ReadToEnd();
                        Stream = JsonConvert.DeserializeObject<Stream>(jsonResponse);
                    }
                }
            }
            catch (WebException ex)
            {
                using (var reader = new StreamReader(ex.Response.GetResponseStream()))
                {
                    string errorJson = reader.ReadToEnd();
                    Error error = JsonConvert.DeserializeObject<Error>(errorJson);
                    Console.WriteLine($"{DateTime.Now:dd-MM-yyyy} - Error getting stream info from Twitch : {error.Status}- {error.Number} : {error.Message}");
                }
            }

            return Stream;
        }

        public static string GetStreamUptime(string _ClientId, string _ChannelName, string _Token)
        {
            Stream stream = GetStreamByChannelName(_ClientId, _ChannelName, _Token);

            StringBuilder message = new StringBuilder();

            if (stream.Data.Count == 0)
            {
                message.Append($"{_ChannelName} is offline right now, come back later ( ˘ ³ ˘ ) *:･ﾟ✧*:･ﾟ✧ ");
            }
            else
            {
                DateTime currentTime = DateTime.Now;

                TimeSpan uptime = currentTime - stream.Data[0].StartedAt.ToLocalTime();
                message.Append($"{_ChannelName} has been live for ");

                if (uptime.Days != 0)
                {
                    if (uptime.Days > 1)
                    {
                        message.Append(string.Format($"{uptime.Days} days "));
                    }
                    else
                    {
                        message.Append(string.Format($"{uptime.Days} day "));
                    }
                }

                if (uptime.Hours != 0)
                {
                    if (uptime.Hours > 1)
                    {
                        message.Append(string.Format($"{uptime.Hours} hours "));
                    }
                    else
                    {
                        message.Append(string.Format($"{uptime.Hours} hour "));
                    }
                }

                if (uptime.Minutes != 0)
                {
                    if (uptime.Minutes > 1)
                    {
                        message.Append(string.Format($"{uptime.Minutes} minutes "));
                    }
                    else
                    {
                        message.Append(string.Format($"{uptime.Minutes} minute "));
                    }
                }

                if (uptime.Seconds != 0)
                {
                    if (uptime.Seconds > 1)
                    {
                        message.Append(string.Format($"{uptime.Seconds} seconds "));
                    }
                    else
                    {
                        message.Append(string.Format($"{uptime.Seconds} second "));
                    }
                }
            }

            return message.ToString();
        }
    }
}
