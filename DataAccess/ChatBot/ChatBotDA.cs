﻿using Models;
using Newtonsoft.Json;
using System;
using System.IO;
using System.Net;
using System.Text;
using Stream = System.IO.Stream;

namespace DataAccess
{
    public static class ChatBotDA
    {
        public static void ValidateOAuth(string _AccessToken)
        {
            string urlValidate = "https://id.twitch.tv/oauth2/validate";

            HttpWebRequest webRequestValidate = (HttpWebRequest)WebRequest.Create(urlValidate);
            if (webRequestValidate != null)
            {
                webRequestValidate.Method = "GET";
                webRequestValidate.Timeout = 12000;
                webRequestValidate.ContentType = "application/json";
                webRequestValidate.Headers.Add($"Authorization: Bearer {_AccessToken}");
            }

            try
            {
                using (Stream s = webRequestValidate.GetResponse().GetResponseStream())
                {
                    using (StreamReader sr = new System.IO.StreamReader(s))
                    {
                        var jsonResponse = sr.ReadToEnd();
                        AuthResponse response = JsonConvert.DeserializeObject<AuthResponse>(jsonResponse);
                    }
                }
            }

            catch (WebException ex)
            {
                using (var reader = new StreamReader(ex.Response.GetResponseStream()))
                {
                    string errorJson = reader.ReadToEnd();
                    Error error = JsonConvert.DeserializeObject<Error>(errorJson);
                    Console.WriteLine($"{DateTime.Now:dd-MM-yyyy} - Error validating token : {error.Status}- {error.Number} : {error.Message}");
                }
            }
        }
        
    }
}
