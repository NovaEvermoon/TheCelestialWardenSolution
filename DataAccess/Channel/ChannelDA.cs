﻿using Newtonsoft.Json;
using System;
using System.IO;
using System.Net;
using Models;
using System.Linq;
using Stream = System.IO.Stream;

namespace DataAccess
{
    public class ChannelDA
    {
        public static Channel GetChannel(string _BroadcasterId, string _ClientId, string _Token)
        {
            Channel Channel = new Channel();
            string Url = $"https://api.twitch.tv/helix/channels?broadcaster_id={_BroadcasterId}";
            HttpWebRequest Request = (HttpWebRequest)WebRequest.Create(Url);

            if (Request != null)
            {
                Request.Method = "GET";
                Request.Timeout = 12000;
                Request.ContentType = "application/json";
                Request.Accept = "Accept: application/vnd.twitchtv.v5+json";
                Request.Headers.Add($"Client-ID: {_ClientId}");
                Request.Headers.Add($"Authorization: Bearer {_Token}");
            }

            try
            {
                using (Stream Stream = Request.GetResponse().GetResponseStream())
                {
                    using (StreamReader StreamReader = new StreamReader(Stream))
                    {
                        string JsonResponse = StreamReader.ReadToEnd();
                        Channel = JsonConvert.DeserializeObject<Channel>(JsonResponse);
                    }
                }

            }
            catch (WebException ex)
            {
                using (var reader = new StreamReader(ex.Response.GetResponseStream()))
                {
                    string errorJson = reader.ReadToEnd();
                    Error error = JsonConvert.DeserializeObject<Error>(errorJson);
                    Console.WriteLine($"{DateTime.Now:dd-MM-yyyy} - Error getting Channel info from Twitch : {error.Status}- {error.Number} : {error.Message}");
                }
            }

            return Channel;
        }

        public static Moderators GetChannelModerators(string _BroadcasterId, string _ClientId, string _Token)
        {
            Moderators moderators = new Moderators();

            string Url = $"https://api.twitch.tv/helix/moderation/moderators?broadcaster_id={_BroadcasterId}";
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(Url);

            if (request != null)
            {
                request.Method = "GET";
                request.Timeout = 12000;
                request.ContentType = "application/json";
                request.Accept = "Accept: application/vnd.twitchtv.v5+json";
                request.Headers.Add($"Client-ID: {_ClientId}");
                request.Headers.Add($"Authorization: Bearer {_Token}");
            }

            try
            {
                using (Stream Stream = request.GetResponse().GetResponseStream())
                {
                    using (StreamReader StreamReader = new StreamReader(Stream))
                    {
                        string JsonResponse = StreamReader.ReadToEnd();
                        moderators = JsonConvert.DeserializeObject<Moderators>(JsonResponse);
                    }
                }

            }
            catch (WebException ex)
            {
                using (var reader = new StreamReader(ex.Response.GetResponseStream()))
                {
                    string errorJson = reader.ReadToEnd();
                    Error error = JsonConvert.DeserializeObject<Error>(errorJson);
                    Console.WriteLine($"{DateTime.Now:dd-MM-yyyy} - Error getting Moderators info from Twitch : {error.Status}- {error.Number} : {error.Message}");
                }
            }

            return moderators;
        }

        public static string GetChannelTitle(string _BroadcasterId, string _ClientId, string _Token)
        {
            Channel channel = GetChannel(_BroadcasterId, _ClientId, _Token);

            string title = $"The stream's current title is : {channel.UserData[0].Title}";

            return title;
        }

        public static string GetChannelGame(string _BroadcasterId, string _ClientId, string _Token)
        {
            Channel channel = GetChannel(_BroadcasterId, _ClientId, _Token);

            string game = $"Currently playing : {channel.UserData[0].GameName}";

            return game;
        }

        public static string GetShoutOut(string _UserName, string _ClientId, string _Token, out User _User)
        {
            string shoutOut;
            _User = new User();

            if (_UserName != "")
            {
                User user = new User();

                _User = UserDA.GetUser(_UserName, _ClientId, _Token);
                Channel channel = GetChannel(_User.Users[0].Id, _ClientId, _Token);

                if (user.Users.Count() != 0)
                {

                    shoutOut = $"✧･ﾟ: ✧･ﾟ: Streamer alert :･ﾟ✧:･ﾟ✧ ! Show some love to this wonderful human being at : http://twitch.tv/{_UserName}, they were last seen streaming {channel.UserData[0].GameName}";

                }
                else
                {
                    shoutOut = "This streamer doesn't exist, make sure you wrote their username correctly!";
                }

            }
            else
            {
                shoutOut = "You didn't specify any streamer for the shoutout";
            }

            return shoutOut;
        }
    }
}
