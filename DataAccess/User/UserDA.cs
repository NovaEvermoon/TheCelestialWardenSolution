﻿using System;
using System.IO;
using System.Net;
using Models;
using Newtonsoft.Json;
using Stream = System.IO.Stream;

namespace DataAccess
{
    public class UserDA
    {
        public static User GetUser(string _Login, string _ClientId, string _Token)
        {
            User user = new User();

            string url = $"https://api.twitch.tv/helix/users?login={_Login}";
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);

            if (request != null)
            {
                request.Method = "GET";
                request.Timeout = 12000;
                request.Headers.Add($"Client-ID: {_ClientId}");
                request.ContentType = "application/json";
                request.Accept = "application/vnd.twitchtv.v5+json";
                request.Headers.Add($"Authorization: Bearer {_Token}");
            }

            try
            {
                using (Stream stream = request.GetResponse().GetResponseStream())
                {
                    using (StreamReader streamReader = new StreamReader(stream))
                    {
                        string jsonresponse = streamReader.ReadToEnd();
                        user = JsonConvert.DeserializeObject<User>(jsonresponse);
                    }
                }
            }
            catch (WebException ex)
            {
                using (var reader = new StreamReader(ex.Response.GetResponseStream()))
                {
                    string errorJson = reader.ReadToEnd();
                    Error error = JsonConvert.DeserializeObject<Error>(errorJson);
                    Console.WriteLine($"{DateTime.Now:dd-MM-yyyy} - Error getting User info from Twitch : {error.Status}- {error.Number} : {error.Message}");
                 }
            }

            return user;
        }

        public static Subscription GetSubscription(string _UserId, string _BroadcasterId, string _ClientId, string _Token)
        {
            Subscription subscription = new Subscription();

            string url = $"https://api.twitch.tv/helix/subscriptions/user?broadcaster_id={_BroadcasterId}&user_id={_UserId}";
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            if (request != null)
            {
                request.Method = "GET";
                request.Timeout = 12000;
                request.Headers.Add("Client-ID", _ClientId);
                request.Headers.Add(String.Format("Authorization: Bearer {0}", _Token));
            }

            try
            {
                using (Stream s = request.GetResponse().GetResponseStream())
                {
                    using (StreamReader sr = new System.IO.StreamReader(s))
                    {
                        var jsonResponse = sr.ReadToEnd();
                        subscription = JsonConvert.DeserializeObject<Subscription>(jsonResponse);
                    }
                }
            }
            catch (WebException ex)
            {
                using (var reader = new StreamReader(ex.Response.GetResponseStream()))
                {
                    string errorJson = reader.ReadToEnd();
                    Error error = JsonConvert.DeserializeObject<Error>(errorJson);
                    Console.WriteLine($"{DateTime.Now:dd-MM-yyyy} - Error getting Subscription info from Twitch : {error.Status}- {error.Number} : {error.Message}");
                }
            }

            return subscription;
        }

        public static string GetFollowage(string _ChatUserName, string _ChatUserId,string _BroadCasterId,string _ClientId, string _Token)
        {
            Followage followage = new Followage();

            DateTime today = DateTime.Now;
            DateTime zeroTime = new DateTime(1, 1, 1);
            string message = "";

            string url = $"https://api.twitch.tv/helix/users/follows?from_id={_ChatUserId}&to_id={_BroadCasterId}";

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            if (request != null)
            {
                request.Method = "GET";
                request.Timeout = 12000;
                request.Headers.Add("Client-ID", _ClientId);
                request.Headers.Add(String.Format("Authorization: Bearer {0}", _Token));
            }

            try
            {
                using (Stream s = request.GetResponse().GetResponseStream())
                {
                    using (StreamReader sr = new System.IO.StreamReader(s))
                    {
                        var jsonResponse = sr.ReadToEnd();
                        followage = JsonConvert.DeserializeObject<Followage>(jsonResponse);
                    }
                }
            }
            catch (WebException ex)
            {
                using (var reader = new StreamReader(ex.Response.GetResponseStream()))
                {
                    string errorJson = reader.ReadToEnd();
                    Error error = JsonConvert.DeserializeObject<Error>(errorJson);
                    Console.WriteLine($"{DateTime.Now:dd-MM-yyyy} - Error getting Followage info from Twitch : {error.Status}- {error.Number} : {error.Message}");
                }
            }

            if (followage.Data != null)
            {
                TimeSpan span = today - followage.Data[0].followed_at;

                int years = (zeroTime + span).Year - 1;
                int months = (zeroTime + span).Month - 1;
                int days = (zeroTime + span).Day;

                message = $"{_ChatUserName} has been following the channel for ";

                if (years > 0 && years < 2)
                {
                    message += years + " year ";
                }
                else if (years >= 2)
                {
                    message += years + " years ";
                }

                if (months > 0 && months < 2)
                {
                    message += months + " month ";
                }
                else if (months >= 2)
                {
                    message += months + " months ";
                }

                if (days > 0 && days < 2)
                {
                    message += days + " day ";
                }
                else if (days >= 2)
                {
                    message += days + " days ";
                }

                message += "! Thank you so much for the continued support (๑′ᴗ‵๑) ♡";
            }
            else
            {
                message = $"{_ChatUserName} is not following the channel";
            }

            return message;
        }
    }
}
